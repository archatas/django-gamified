============
Installation
============

At the command line::

    $ easy_install django-gamified

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv django-gamified
    $ pip install django-gamified
