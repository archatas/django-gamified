# -*- coding: UTF-8 -*-
from __future__ import unicode_literals


def add_points(owner, skill_type_slug, points):
    """
    Adds skill points to the owner
    :param owner: An object of content type defined in GAMIFIED_SKILLPOINTS_OWNER_CONTENT_TYPE_FILTER,
                  usually at least the user
    :param skill_type_slug: the slug of the SkillType
    :param points: whole number of points to add
    :return: SkillPoints instance
    """
    from django.contrib.contenttypes.models import ContentType
    from .models import SkillType, SkillPoints

    ct = ContentType.objects.get_for_model(owner)
    skill_type, _created = SkillType.objects.get_or_create(
        slug=skill_type_slug,
        content_type=ct,
        defaults={
            "title": skill_type_slug,
        },
    )
    skill_points, _created = SkillPoints.objects.get_or_create(
        skill_type=skill_type,
        content_type=ct,
        object_id=owner.pk,
    )
    skill_points.points += points
    skill_points.save()
    return skill_points


def subtract_points(owner, skill_type_slug, points):
    """
    Subtracts skill points from the owner
    :param owner: An object of content type defined in GAMIFIED_SKILLPOINTS_OWNER_CONTENT_TYPE_FILTER,
                  usually at least the user
    :param skill_type_slug: the slug of the SkillType
    :param points: whole number of points to add
    :return: SkillPoints instance
    """
    return add_points(owner, skill_type_slug, -points)
