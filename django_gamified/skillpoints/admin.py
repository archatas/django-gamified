# -*- coding: UTF-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.contenttypes.admin import GenericStackedInline

from .models import SkillType, SkillPoints


@admin.register(SkillType)
class SkillTypeAdmin(admin.ModelAdmin):
    fields = ("title", "slug", "content_type")
    list_display = ("title", "slug", "content_type", "created", "modified")
    list_filter = ("content_type", "created", "modified")
    ordering = ("title", "content_type__model")


@admin.register(SkillPoints)
class SkillPointsAdmin(admin.ModelAdmin):
    fields = ("skill_type", "content_type", "object_id", "content_object", "points")
    list_display = (
        "content_object",
        "content_type",
        "skill_type",
        "points",
        "created",
        "modified",
    )
    list_filter = ("content_type", "skill_type", "created", "modified")
    readonly_fields = ("content_object",)
    ordering = ("-created",)


class SkillPointsInline(GenericStackedInline):
    model = SkillPoints
    fields = ("skill_type", "points")
    extra = 0
    can_delete = True

    def get_max_num(self, request, obj=None, **kwargs):
        return SkillType.objects.count()
