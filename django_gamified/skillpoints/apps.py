from __future__ import unicode_literals

from django.apps import AppConfig


class SkillpointsConfig(AppConfig):
    name = "django_gamified.skillpoints"
