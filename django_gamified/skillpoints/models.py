# -*- coding: UTF-8 -*-
from __future__ import unicode_literals

import uuid

from django.db import models
from django.utils.encoding import force_text
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes.fields import GenericForeignKey
from django.conf import settings

OWNER_CONTENT_TYPES = getattr(
    settings,
    "GAMIFIED_SKILLPOINTS_OWNER_CONTENT_TYPE_FILTER",
    models.Q(app_label="auth", model="user"),
)


class SkillType(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created = models.DateTimeField(
        _("Creation date and time"), auto_now_add=True, editable=False
    )
    modified = models.DateTimeField(
        _("Modification date and time"), auto_now=True, editable=False
    )
    title = models.CharField(_("Title"), max_length=200)
    slug = models.SlugField(_("Slug"), max_length=200)
    content_type = models.ForeignKey(
        "contenttypes.ContentType",
        verbose_name=_("Applicable for content type"),
        limit_choices_to=OWNER_CONTENT_TYPES,
        on_delete=models.CASCADE,
    )

    class Meta:
        verbose_name = _("Skill Type")
        verbose_name_plural = _("Skill Types")
        ordering = ("-created",)

    def __str__(self):
        return self.title


class SkillPoints(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created = models.DateTimeField(
        _("Creation date and time"), auto_now_add=True, editable=False
    )
    modified = models.DateTimeField(
        _("Modification date and time"), auto_now=True, editable=False
    )
    skill_type = models.ForeignKey(
        SkillType, verbose_name=_("Skill type"), on_delete=models.CASCADE
    )
    content_type = models.ForeignKey(
        "contenttypes.ContentType",
        verbose_name=_("Owner type"),
        limit_choices_to=OWNER_CONTENT_TYPES,
        on_delete=models.CASCADE,
    )
    object_id = models.CharField(
        _("Owner ID"),
        max_length=255,
        help_text=_("Please enter the ID of the skill points' owner."),
    )
    points = models.BigIntegerField(_("Points"), default=0)

    content_object = GenericForeignKey("content_type", "object_id")

    class Meta:
        verbose_name = _("Skill Points")
        verbose_name_plural = _("Skill Points")
        unique_together = ("skill_type", "content_type", "object_id")
        ordering = ("-created",)

    def __str__(self):
        return force_text(self.content_object)
