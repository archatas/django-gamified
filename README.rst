=============================
django-gamified
=============================

.. image:: https://badge.fury.io/py/django-gamified.png
    :target: https://badge.fury.io/py/django-gamified

.. image:: https://travis-ci.org/archatas/django-gamified.png?branch=master
    :target: https://travis-ci.org/archatas/django-gamified

Gamification app for Django projects

Documentation
-------------

The full documentation is at https://django-gamified.readthedocs.org.

Quickstart
----------

Install django-gamified::

    pip install django-gamified

Then use it in a project::

    import django_gamified

Features
--------

* TODO

Running Tests
--------------

Does the code actually work?

::

    source <YOURVIRTUALENV>/bin/activate
    (myenv) $ pip install -r requirements_test.txt
    (myenv) $ python runtests.py

Credits
---------

Tools used in rendering this package:

*  Cookiecutter_
*  `cookiecutter-djangopackage`_

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`cookiecutter-djangopackage`: https://github.com/pydanny/cookiecutter-djangopackage
